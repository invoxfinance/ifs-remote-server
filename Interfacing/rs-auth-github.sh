# Invox Finance passport management with github

# Setup the Environment variables for the REST Server

# need npm install -g passport-github

#1. Set up the card to be used
export COMPOSER_CARD=admin@ifs-network

#2. Set up the namespace usage    always |  never
export COMPOSER_NAMESPACES=always

#3. Set up the REST server Authhentcation    true | false
export COMPOSER_AUTHENTICATION=true

#4. Set up the Passport strategy provider
export COMPOSER_PROVIDERS='{
  "google": {
    "provider": "google",
    "module": "passport-google-oauth2",
    "clientID": "1053141338771-4dsr21dsiro8grqm59tpu9rqfuqmeqo6.apps.googleusercontent.com",
    "clientSecret": "kZ2H4_acIFJHmfqkaT6_Zpxj",
    "authPath": "/auth/google",
    "callbackURL": "/auth/google/callback",
    "scope": "https://www.googleapis.com/auth/plus.login",
    "successRedirect": "http://invoxfinance.azurewebsites.net/",
    "failureRedirect": "/"
  },

    "google-appbypass": {
    "provider": "google",
    "module": "passport-google-oauth2",
    "clientID": "1053141338771-4dsr21dsiro8grqm59tpu9rqfuqmeqo6.apps.googleusercontent.com",
    "clientSecret": "kZ2H4_acIFJHmfqkaT6_Zpxj",
    "authPath": "/auth/google-appbypass",
    "callbackURL": "https://invoxfinance.ngrok.io/auth/google-appbypass/callback",
    "scope": "https://www.googleapis.com/auth/plus.login",
    "successRedirect": "/",
    "failureRedirect": "/"
  },

  "github": {
    "provider": "github",
    "module": "passport-github",
    "clientID": "a02ee0f87c152d850642",
    "clientSecret": "5758c8402036dce134729310f86d1afd9c114f01",
    "authPath": "/auth/github/",
    "callbackURL": "/auth/github/callback",
    "successRedirect": "/",
    "failureRedirect": "/",
    "redirect_uri": "https://invoxfinance.ngrok.io/auth/github/callback"
  }

}'

#5. Execute the REST server
composer-rest-server