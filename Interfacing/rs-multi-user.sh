#!/bin/bash

# This script sets up the environment property for 
# Mongo DB loopback connector. This property is used
# by REST server for connecting with the MongoDB 
# instance in the cloud | local

# need  npm install -g loopback-connector-mongodb and brew install mongodb --with-openssl

# mongo "mongodb://restauth-shard-00-00-xkjcv.mongodb.net:27017,restauth-shard-00-01-xkjcv.mongodb.net:27017,
# restauth-shard-00-02-xkjcv.mongodb.net:27017/test?replicaSet=restauth-shard-0" --ssl --authenticationDatabase admin 
# --username test --password <PASSWORD>

#1. Set up the REST server to multi user mode    true | false
export COMPOSER_MULTIUSER=true

# PLEASE CHANGE THIS TO point to your DB instance
# ================================================
# HOST = DB Server host,   PORT = Server port#
# database = Name of the database
# Credentials =>    user/password 
# connector   =>    We are using mongodb, it can be 
#                   any nosql database

export COMPOSER_DATASOURCES='{
    "db": {
        "name": "db",
        
        "host": "restauth-shard-00-00-xkjcv.mongodb.net",
        "port": 27017,
        "url": "mongodb+srv://test:test@restauth-xkjcv.mongodb.net/test",
        "database": "restauth",
        "user": "test",
        "password": "test",
        "connector": "mongodb"  
    }
}'

# Execute the script for enabling authentication
./rs-auth-github.sh 

# cd to main file (tutorial-network) then use the below command to assign and ID to a participant
# composer identity issue -u seller -a org.example.mynetwork.Seller#seller -c admin@tutorial-network

